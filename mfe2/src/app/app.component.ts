import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'mfe2';

    private REST_API_SERVER = "http://localhost:3004/api/bff/test";

    public response: any;

    public username: string = "";

    public constructor(private httpClient: HttpClient) {

    }

    public getServiceResponse() {
        console.log("testing");
        if (this.username == "testing") {
            this.httpClient.get(this.REST_API_SERVER, {
                headers: { 'team': 'sqe' }
            })
                .subscribe((data: any) => {
                    console.log(data);
                    this.response = data;
                });
        }
        else {
            this.httpClient.get(this.REST_API_SERVER)
                .subscribe((data: any) => {
                    console.log(data);
                    this.response = data;
                });
        }

    }
}
